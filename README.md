# AWS Auto Scaling Plans Connector
Use AWS Auto Scaling to create scaling plans for your applications to automatically scale your scalable AWS resources.

Documentation: https://docs.aws.amazon.com/autoscaling/plans/APIReference/Welcome.html

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/autoscaling-plans/2018-01-06/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

